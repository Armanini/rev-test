import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import _ from 'lodash'
Vue.use(Vuex)

const stackSize = process.env.STACK_SIZE || 10;
export default new Vuex.Store({
  state: {
    objectsList: [],
    imageList: []
  },
  getters: {
    getObjectList: (state => {
      return state.objectsList;
    })
  },
  mutations: {
    setObjectList(state, list){
      state.objectsList = list;
    },
    fillImages(state, list){
      state.imageList.push(...list)
    }
  },
  actions: {
    prepareObjectLists({ commit }){
      return new Promise((resolve, reject) => {
        axios.get('https://collectionapi.metmuseum.org/public/collection/v1/search?hasImages=true&q=*').then(({data}) => {
          commit("setObjectList", data.objectIDs);
          resolve(data.objectIDs);
        }).catch((e) => {
          commit("setObjectList", []);
          reject(e)
        })
      });
    },
    // eslint-disable-next-line no-unused-vars
    getNextImage({ commit, state, dispatch }){
      return new Promise((resolve, reject) => {
        if(state.imageList.length>0){
          resolve(state.imageList[0])
          _.pullAt(state.imageList,0)
          dispatch('fillImageList',stackSize)
        }else{
          if(state.objectsList.length===0){
            dispatch('initAll').then((image)=>{
              resolve(image)
            }).catch((e)=>{
              reject(e)
            })
          }else{
            dispatch('initImageFiller').then((image)=>{
              resolve(image)
            }).catch((e)=>{
              reject(e)
            })
          }
        }
      });
    },
    initAll({dispatch}) {
      return new Promise((resolve,reject) => {
        dispatch('prepareObjectLists').then(()=>{
          dispatch('initImageFiller').then((image)=>{
            resolve(image)
          })
        }).catch((e)=>{
          reject(e)
        })
      })
    },
    initImageFiller({dispatch}) {
      return new Promise((resolve,reject) => {
        dispatch('fillImageList',1).then(()=>{
          resolve(this.state.imageList[0])
        }).catch(e=>reject(e))
        dispatch('fillImageList',stackSize)
      })
    },
    fillImageList({state, commit},stackSize) {
      return new Promise( (resolve,reject) => {
        let count = stackSize - state.imageList.length;
        if (count > 0) {
          let nextStackIds = _.shuffle(state.objectsList).splice(0, count)
          let requests = _.map(nextStackIds, (id) => axios.get(`https://collectionapi.metmuseum.org/public/collection/v1/objects/${id}`))
          Promise.allSettled(requests).then(result=>{
            let images = _.map(_.filter(result, {status: 'fulfilled'}), 'value.data');
            commit("fillImages", images);
            resolve()
          }).catch(e=>reject(e))
        }else{
          resolve()
        }
      })
    }
  },
  modules: {
  }
})
